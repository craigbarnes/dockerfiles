#!/bin/sh
set -eu

tree="${1:-$(pwd)/LUACOV}"

for V in 5.1 5.2 5.3 5.4; do
    for P in luacov cluacov luacov-reporter-lcov md5; do
        luarocks --tree="$tree" --lua-version="$V" install "$P"
    done
    mv "$tree/bin/luacov" "$tree/bin/luacov-$V"
done

ln -s luacov-5.4 "$tree/bin/luacov"
