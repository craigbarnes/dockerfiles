FROM registry.gitlab.com/craigbarnes/dockerfiles/cdev-arch:latest

RUN \
    pacman -Syu --noconfirm \
    && pacman -S --noconfirm --needed lcov appstream \
    && pacman -Scc --noconfirm

COPY install-luacov.sh /root/
RUN cd /root/ && sh install-luacov.sh /usr
