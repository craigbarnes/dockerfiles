#!/bin/sh
set -e

export DEBIAN_FRONTEND=noninteractive
apt-get update

apt-get -y --no-install-recommends install \
    make gcc clang tcc ccache gperf ragel pkg-config musl-tools git valgrind \
    gcc-arm-linux-gnueabihf libc-dev-armhf-cross \
    gcc-aarch64-linux-gnu libc-dev-arm64-cross \
    lua5.3 lua5.2 lua5.1 luajit luarocks lua-sec \
    liblua5.3-dev liblua5.2-dev liblua5.1-dev libluajit-5.1-dev \
    libncurses5-dev libssl-dev ca-certificates \
    tar zip curl gnupg tree \
    pandoc doxygen graphviz lcov gcovr \
    shellcheck codespell appstream-util desktop-file-utils

apt-get clean
rm -rf /var/lib/apt/lists/*
