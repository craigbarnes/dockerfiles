#!/bin/sh
set -e

dnf -y --allowerasing install \
    make gcc clang ccache gperf ragel git curl tar \
    valgrind tree groff texinfo pkgconfig \
    glibc-static ncurses-devel lua-devel lua-libs

dnf clean all
