FROM debian:latest

ENV NDK_ARCH linux-x86_64
ENV NDK_RELEASE android-ndk-r26d
ENV NDK_ZIPFILE ${NDK_RELEASE}-linux.zip
ENV NDK_PREFIX /opt/android-ndk
ENV NDK_TOOLCHAINS ${NDK_PREFIX}/toolchains/llvm/prebuilt/${NDK_ARCH}/bin

ENV CC_ARMV7A ${NDK_TOOLCHAINS}/armv7a-linux-androideabi28-clang
ENV CC_AARCH64 ${NDK_TOOLCHAINS}/aarch64-linux-android28-clang
ENV CC_I686 ${NDK_TOOLCHAINS}/i686-linux-android28-clang
ENV CC_X86_64 ${NDK_TOOLCHAINS}/x86_64-linux-android28-clang
ENV CC ${CC_AARCH64}

ENV DEBIAN_FRONTEND noninteractive

RUN \
    apt-get update \
    && apt-get -y --no-install-recommends install \
        make git unzip tree curl ca-certificates \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && curl -LO "https://dl.google.com/android/repository/${NDK_ZIPFILE}" \
    && unzip "${NDK_ZIPFILE}" \
    && rm -f "${NDK_ZIPFILE}" \
    && mkdir -p /opt/ \
    && mv "${NDK_RELEASE}" "${NDK_PREFIX}" \
    && $CC -v
