FROM rockylinux:8

COPY dnf-install.sh /root/

RUN \
    dnf -y install dnf-plugins-core \
    && dnf config-manager --set-enabled powertools \
    && dnf -y install epel-release \
    && sh /root/dnf-install.sh
