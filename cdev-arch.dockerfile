FROM archlinux:latest

RUN \
    pacman -Syu --noconfirm \
    && pacman -S --noconfirm --needed \
        make gcc clang tcc ccache git pkgconf gperf ragel musl \
        curl tree ncurses valgrind pandoc gcovr python-pygments \
        shellcheck codespell desktop-file-utils \
        lua{,53,52,51,jit,rocks} \
        lua{,53,52,51}-{lpeg,filesystem,socket,sec} \
    && pacman -Scc --noconfirm
