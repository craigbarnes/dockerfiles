FROM rockylinux:9

COPY dnf-install.sh /root/

RUN \
    dnf -y install dnf-plugins-core \
    && dnf config-manager --set-enabled crb \
    && dnf -y install epel-release \
    && sh /root/dnf-install.sh
