FROM alpine:latest

RUN apk --no-cache add \
    make gcc clang ccache binutils gperf ragel pkgconf git valgrind \
    libc-utils curl gzip tar tree ncurses gcovr gawk \
    libc-dev ncurses-dev lua5.4-dev lua5.3-dev lua5.2-dev lua5.1-dev
